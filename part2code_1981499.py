# -*- coding: utf-8 -*-
"""CW1_part2.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1a-tvRSqWytS0VsqM_4nclcsR3WKwvBHn

##Import required dependencies
"""

# Required imports and downloads
import pandas as pd
import numpy as np
import sklearn
import nltk
import operator
import re

from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.linear_model import SGDClassifier
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import StandardScaler
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from nltk.sentiment.vader import SentimentIntensityAnalyzer 

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
nltk.download('vader_lexicon')

"""## Load the data"""

# Files imports
# from google.colab import drive
# drive.mount('/content/drive/')

# from google.colab import drive

# PATH HAS TO BE CHANGED!!!
path= '/home/yoolek/Documents/sentiment_imdb/IMDb/'
pathTrain= path+'train/'
pathTest= path+'test/'
pathDev= path+'dev/'

train_positive=open(pathTrain+'imdb_train_pos.txt').readlines()
train_negative=open(pathTrain+'imdb_train_neg.txt').readlines()

dev_positive=open(pathDev+'imdb_dev_pos.txt').readlines()
dev_negative=open(pathDev+'imdb_dev_neg.txt').readlines()

test_positive=open(pathTest+'imdb_test_pos.txt').readlines()
test_negative=open(pathTest+'imdb_test_neg.txt').readlines()

"""#Functions for text preprocessing

##Techniques

*   tokenization
*   stopwords removal
*   normalization
  *   stemming
  *   lemmatization
*   lowercasing
"""

# Data normalization 

# Stemming
stemmer = nltk.stem.porter.PorterStemmer()
def get_stemmed_token(string):
  return stemmer.stem(string)

# Lemmatization
lemmatizer = nltk.stem.WordNetLemmatizer()
def get_lemmatized_token(string):
  return lemmatizer.lemmatize(string);

# Transforms emoticons
def transform_emoticons(string):
  string = re.sub("\W(;\)|;[dD]|:\)|:[dD])\W", "smile ", string)
  string = re.sub("\W(;/|:/|:\(|;\()\W", "sad ", string)
  return string

# Remove URLs from string
def remove_urls(string):
  return re.sub(r"http\S+", "", string)

# Remove <br> tags from string
def remove_br_tags(string):
  return re.sub("<br\s?\/>|<br>", "", string)

# Remove number from string
def remove_numbers(string):
  return re.sub("\w*\d\w*", "", string) #remove strings that contain numbers

# set stopwords
stopwords=set(nltk.corpus.stopwords.words('english'))
# added extra stopwords to remove unwanted punctuation
stopwords.add(".")
stopwords.add(",")
stopwords.add("--")
stopwords.add("``")
stopwords.add("-")
stopwords.add("&")
stopwords.add(";")
stopwords.add("'")
stopwords.add("''")
stopwords.add("(")
stopwords.add(")")
stopwords.add(":")
stopwords.add(";")

# Normalizes data
# returns clean dataset
def normalize_data(data):
  normalized_data = []
  for string in data:
    clean_string = transform_emoticons(remove_urls(remove_br_tags(remove_numbers(string))))
    tokens = get_tokens(clean_string)
    list_tokens=[]
    for token in tokens:
      token = get_lemmatized_token(token.lower())
      if token == "n't":
        token = "not"
      if token not in stopwords:
        list_tokens.append(token)
    normalized_data.append(' '.join(list_tokens))
  return normalized_data

# Tokenizes string      
def get_tokens(string):
  return nltk.tokenize.word_tokenize(string)

# Returns sorted array of most frequent words in the dataset
# number - max number of words
def get_frequent_words(data, number):
  word_frequency={}
  for string in data:
    tokens=get_tokens(string)
    for token in tokens:
      if token not in word_frequency: word_frequency[token]=1
      else: word_frequency[token]+=1

  #sorts a list from most frequent word to least frequent; number - list length
  list = sorted(word_frequency.items(), key=operator.itemgetter(1), reverse=True)[:number]
  return [word for word,frequency in list]

# One hot encoder
# returns vector indicating appearance of a token in 'words'
# words - array of tokens
def get_onehot_vector(string, words):
  vector=np.zeros(len(words))
  tokens=get_tokens(string)
  for i, word in enumerate(words):
    if word in tokens:
      # if word in given sentence, update its appearance in vector array
      vector[i]=1
  return vector

"""##Transformers"""

## Transformer classes

# returns array of normalized word lengths of reviews
class CountLength(BaseEstimator, TransformerMixin):
  def fit(self, X, y=None):
    return self
  def transform(self, X, y=None):
    review_lengths = []
    for sentence in X:
      review_lengths.append([len(get_tokens(sentence))])
    scaler = StandardScaler() # normalizes values
    return scaler.fit_transform(review_lengths)

# returns vectors indicating appearance of a token 
# in the list of most frequent words
class OneHot(BaseEstimator, TransformerMixin):
  def fit(self, X, y=None):
    return self
  def transform(self, X, y=None):
    words = get_frequent_words(X, 5000)
    vectors = []
    for sentence in X:
      vectors.append(get_onehot_vector(sentence, words))
    return vectors

# returns array of compund scores of polarity of each review
class Polarity(BaseEstimator, TransformerMixin):
  def fit(self, X, y=None):
    return self
  def transform(self, X, y=None):
    sen_obj = SentimentIntensityAnalyzer() 
    scores = []
    for sentence in X:
      compound = sen_obj.polarity_scores(sentence)['compound']
      scores.append([compound]) 
    return scores

# Merge positive and negative reviews
def merge_pos_neg(pos_reviews, neg_reviews):
  pos=normalize_data(pos_reviews)
  neg=normalize_data(neg_reviews)
  X=[]
  Y=[]
  for review in pos:
    X.append(review)
    Y.append(1)
  for review in neg:
    X.append(review)
    Y.append(0)
  return X, Y

X_train, Y_train = merge_pos_neg(train_positive, train_negative)
X_dev, Y_dev = merge_pos_neg(dev_positive, dev_negative)
X_test, Y_test = merge_pos_neg(test_positive, test_negative)

"""## Pipeline

* **unigrams** - CountVectorizer
* **bigrams** - TfidfVectorizer (range (2,2)), SelectKBest - chi2
* **polarity** - Polarity using SentimentIntensityAnalyzer
"""

# sets and returns pipeline
def get_pipeline(settings):
  # SETTINGS
  MAX_FEATURES = settings['MAX_FEATURES']
  BEST_K = settings['BEST_K']
  MIN_DF = settings['MIN_DF']
  print(MAX_FEATURES, BEST_K, MIN_DF)
  # Pipeline
  return Pipeline([
    ('features', FeatureUnion([
        ('unigrams', Pipeline([
          ('occurances', CountVectorizer(max_features=MAX_FEATURES, min_df=MIN_DF)),
        ])),
        ('bigrams', Pipeline([
          ('tfidf', TfidfVectorizer(max_features=MAX_FEATURES, ngram_range=(2,2))),
          ('kbest', SelectKBest(chi2, k=BEST_K)),              
        ])),
        ('pos_neg', Pipeline([
          ('polarity', Polarity()),
        ])),
      ],
    )),
    ('clf', SGDClassifier())
  ])

settings = {
  'MAX_FEATURES': 5000,
  'BEST_K': 2500,
  'MIN_DF': 0.01
}

pipeline = get_pipeline(settings)
# fits the train data, gets the features and trains classifier
pipeline.fit(X_train, Y_train)
# predicts
Y_predicted = pipeline.predict(X_test)

# classification report
print(classification_report(Y_test, Y_predicted))

# confusion matrix
print (confusion_matrix(Y_test, Y_predicted))

print(accuracy_score(Y_test, Y_dev_predictions))
print(precision_score(Y_test, Y_dev_predictions, average='macro'))
print(recall_score(Y_test, Y_dev_predictions, average='macro'))
print(f1_score(Y_test, Y_dev_predictions, average='macro'))

"""## Parameters tuning on dev set"""

from sklearn.metrics import precision_score,recall_score,f1_score,accuracy_score

# parameters to test out
list_settings =[{
  'MAX_FEATURES': 10000,
  'BEST_K': 5000,
  'MIN_DF': 2
}, # accuracy 0.858
{
  'MAX_FEATURES': 2000,
  'BEST_K': 1000,
  'MIN_DF': 2
}, # accuracy 0.856
{
  'MAX_FEATURES': 5000,
  'BEST_K': 2500,
  'MIN_DF': 0.01
}, # accuracy 0.863
{
  'MAX_FEATURES': 5000,
  'BEST_K': 750,
  'MIN_DF': 0.2
}, # accuracy 0.777
{
  'MAX_FEATURES': 2000,
  'BEST_K': 500,
  'MIN_DF': 2
} # accuracy 0.857 
] 


best_accuracy_dev=0.0
i=0
# tests each settings on a dev set
for settings in list_settings:
  pipeline = get_pipeline(settings) # creates pipeline
  pipeline.fit(X_train, Y_train) # fits data
  Y_dev_predictions=pipeline.predict(X_dev) # predicts on dev set
  accuracy_dev=accuracy_score(Y_dev, Y_dev_predictions) # calculates accuracy score
  i+=1
  # prints out accuracy for each setting
  print (str(round(accuracy_dev,3)))
  if accuracy_dev>=best_accuracy_dev:
    best_accuracy_dev=accuracy_dev
    best_num_features=settings
print ("Best accuracy overall ",str(round(best_accuracy_dev,3))," with ", str(best_num_features))