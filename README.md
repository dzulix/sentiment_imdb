# IMDb reviews - Sentiment Analysis

## Project Overview
Project has been developed as part of an assesed coursework at the university. The project's purpose is to perform sentiment analysis of IMDb reviews in Python. Data has been preprocessed using different techniques such as tokenization, lemmatization, lowercasing, stopwords removal.
The classifier is trained on three different features extracted during the feature engineering process. The SVM classifier has been used to build the model.

## Installation
In order to:

* launch a Jupyter Notebook `part2code_1981499.ipynb` use this command `jupyter notebook [filename].ipynb` or upload it to for example [Google Colab](https://colab.research.google.com/). 

* run .py script use following command `python3 [filename].py`. **This requires to install all the required dependecies.**

*The project was developed in Jupyter Notebook and running the .py script was not tested.*

Remember to download the dataset files from the IMDb folder and update the paths in the script(s) accordingly to where you place your files.


